#include <stdio.h>
#include <stdlib.h>
#include "graphe.h"
#include <sys/wait.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
typedef enum {ROUGE=0, BLEU=1, VERT=2} tCouleur;
typedef tCouleur tTabCouleurs[MAX_SOMMETS];

int get_greens(tTabCouleurs tab, int greens[MAX_SOMMETS])
{
    int i;
    int idx = -1;

    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        greens[i] = -1;
    }
    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        if (tab[i] == VERT)
        {
            idx++;
            greens[idx] = i;
        }
    }

    return idx;
}

float get_minimal_pi(float pi[MAX_SOMMETS], int greens[MAX_SOMMETS])
{
    int i;
    int idx;
    float pi_value = -1;
    int pi_idx = -1;

    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        idx = greens[i];
        if (idx != -1)
        {
            if (pi[idx] < pi_value || pi_value == -1)
            {
                pi_idx = idx;
                pi_value = pi[idx];
            }
        }
    }
    return pi_idx;
}

void dijkstra_algorithm(tGraphe graphe, tNumeroSommet root)
{
    int i;
    int flag = 0;
    float test;
    int x;
    tTabCouleurs tab;
    float pi[MAX_SOMMETS]; 
    int greens[MAX_SOMMETS];
    tNumeroSommet pred[MAX_SOMMETS];
    tNumeroSommet successeur;

    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        pi[i] = -1;
        pred[i] = -1;
        tab[i] = BLEU;
    }

    tab[root] = VERT;
    pi[root] = 0;

    flag = get_greens(tab, greens);

    while (flag != -1)
    {
        x = get_minimal_pi(pi, greens);
        tab[x] = ROUGE;
        for (i = 0; i < grapheNbSuccesseursSommet(graphe, x); ++i)
        {
            successeur = grapheSuccesseurSommetNumero(graphe, x, i);
            test = pi[x] + grapheRecupValeurArc(graphe, x, successeur);
            if (tab[successeur] == BLEU || (tab[successeur] == VERT && pi[successeur] > test))
            {
                tab[successeur] = VERT;
                pi[successeur] = test;
                pred[successeur] = x;
            }
        }
        flag = get_greens(tab, greens);
    }

    for (i = 0; i < MAX_SOMMETS; i++)
    {
        if (pi[i] != -1)
            printf(" %f", pi[i]);
    }
    printf("\n");
    for (i = 0; i < MAX_SOMMETS; i++)
    {
        if (pred[i] != -1 || i == root)
            printf("%d ", pred[i]);
    }
    printf("\n");

}

tPileSommets existsPathBetween(tGraphe graphe, tNumeroSommet start, tNumeroSommet end)
{
    int i;
    int flag = 0;
    tTabCouleurs tab;
    tPileSommets stack;
    tNumeroSommet next;
    tNumeroSommet neighbour;
    tNomSommet name;

    for (i = 0; i < MAX_SOMMETS; ++i)
        tab[i] = BLEU;

    stack = pileSommetsAlloue();

    tab[start] = VERT;
    pileSommetsEmpile(stack, start);

    while (!pileSommetsEstVide(stack))
    {
        next = pileSommetsTete(stack);

        if (next == end)
            return stack;

        grapheRecupNomSommet(graphe, next, name);
        flag = 0;

        for (i = 0; i < grapheNbVoisinsSommet(graphe, next); ++i)
        {
            neighbour = grapheVoisinSommetNumero(graphe, next, i);
            if (tab[neighbour] == BLEU)
            {
                pileSommetsEmpile(stack, neighbour);
                tab[neighbour] = VERT;
                flag = 1;
                break;
            }
        }

        if (flag == 0)
        {
            tab[next] = ROUGE;
            pileSommetsDepile(stack);
        }
    }
    return NULL;
}

void depthFirstSearch(tGraphe graphe, tNumeroSommet start)
{
    int i;
    int flag = 0;
    tTabCouleurs tab;
    tPileSommets stack;
    tNumeroSommet next;
    tNumeroSommet neighbour;
    tNomSommet name;

    for (i = 0; i < MAX_SOMMETS; ++i)
        tab[i] = BLEU;

    stack = pileSommetsAlloue();

    tab[start] = VERT;
    pileSommetsEmpile(stack, start);

    while (!pileSommetsEstVide(stack))
    {
        next = pileSommetsTete(stack);

        grapheRecupNomSommet(graphe, next, name);
        flag = 0;
        printf("%s\n", name);

        for (i = 0; i < grapheNbVoisinsSommet(graphe, next); ++i)
        {
            neighbour = grapheVoisinSommetNumero(graphe, next, i);
            if (tab[neighbour] == BLEU)
            {
                pileSommetsEmpile(stack, neighbour);
                tab[neighbour] = VERT;
                flag = 1;
                break;
            }
        }

        if (flag == 0)
        {
            tab[next] = ROUGE;
            pileSommetsDepile(stack);
        }
    }
}

void shortestPath(tGraphe graphe, tNumeroSommet sommet)
{
    tTabCouleurs tab;
    int i;
    tFileSommets queue;
    tNumeroSommet nextSommet;
    tNumeroSommet voisin;
    int d[MAX_SOMMETS];
    tNumeroSommet pred[MAX_SOMMETS];

    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        tab[i] = BLEU;
        d[i] = -1;
        pred[i] = -1;
    }

    queue = fileSommetsAlloue();

    fileSommetsEnfile(queue, sommet);
    tab[sommet] = VERT;
    d[sommet] = 0;

    while (!fileSommetsEstVide(queue))
    {
        nextSommet = fileSommetsDefile(queue);
        for (i = 0; i < grapheNbVoisinsSommet(graphe, nextSommet); ++i)
        {
            voisin = grapheVoisinSommetNumero(graphe, nextSommet, i);
            if (tab[voisin] == BLEU)
            {
                tab[voisin] = VERT;
                fileSommetsEnfile(queue, voisin);
                d[voisin] = d[nextSommet] + 1;
                pred[voisin] = nextSommet;
            }
        }
        tab[nextSommet] = ROUGE;
    }
    /*
       for (i = 0; i < MAX_SOMMETS; i++)
       {
       if (d[i] != -1)
       printf(" %d", d[i]);
       }
       printf("\n");

       for (i = 0; i < MAX_SOMMETS; i++)
       {
       if (pred[i] != -1 || i == sommet)
       printf("%d ", pred[i]);
       }
       printf("\n");
       */
}

void testShortestPath(tGraphe graphe, int n, int m)
{
    grapheAleatoire(graphe, n, 1, m);
    shortestPath(graphe, 0);
}


int getNbNextDependingOnOrientation(tGraphe graphe, tNumeroSommet sommet)
{
    if (grapheEstOriente(graphe))
        return grapheNbSuccesseursSommet(graphe, sommet);
    else
        return grapheNbVoisinsSommet(graphe, sommet);
}

tNumeroSommet getNextDependingOnOrientation(tGraphe graphe, tNumeroSommet sommet, int index)
{
    if (grapheEstOriente(graphe))
        return grapheSuccesseurSommetNumero(graphe, sommet, index);
    else
        return grapheVoisinSommetNumero(graphe, sommet, index);
}


void breadth_first_search(tGraphe graphe, tNomSommet sommet)
{
    int i;
    tFileSommets queue;
    tNomSommet sommetName;
    tNumeroSommet sommetIdx;
    tNumeroSommet voisinIdx;

    tTabCouleurs tab;

    for (i = 0; i < MAX_SOMMETS; ++i)
    {
        tab[i] = BLEU;
    }

    queue = fileSommetsAlloue();

    sommetIdx = grapheChercheSommetParNom(graphe, sommet);

    fileSommetsEnfile(queue, sommetIdx);
    tab[sommetIdx] = VERT;

    while (!fileSommetsEstVide(queue))
    {
        sommetIdx = fileSommetsDefile(queue);
        for (i = 0; i < getNbNextDependingOnOrientation(graphe, sommetIdx); ++i)
        {
            voisinIdx = getNextDependingOnOrientation(graphe, sommetIdx, i);
            if (tab[voisinIdx] == BLEU) 
            {
                fileSommetsEnfile(queue, voisinIdx);
                tab[voisinIdx] = VERT;
            }
        }
        tab[sommetIdx] = ROUGE;
        grapheRecupNomSommet(graphe, sommetIdx, sommetName);
        printf("%s\n", sommetName);
    }

    fileSommetsLibere(queue);
}


void graphe_to_visual(tGraphe graphe, char *outfile) {
    FILE *fic;
    char commande[80];
    char dotfile[80];
    int ret;

    tNomSommet nomSommetOrig;
    tNomSommet nomSommetDest;
    char* separator;
    char* grapheType;

    int i;

    /* on va creer un fichier pour graphviz, dans le fichier "outfile".dot */
    strcpy(dotfile, outfile);
    strcat(dotfile, ".dot");
    fic = fopen(dotfile, "w");

    if (fic == NULL)
        halt ("Ouverture du fichier %s en écriture impossible\n", dotfile);

    if (grapheEstOriente(graphe))
    {
        separator = "->";
        grapheType = "digraph {";
    }
    else
    {
        separator = "--";
        grapheType = "graph {";
    }

    fprintf(fic, "%s \n", grapheType);

    for (i = 0; i < grapheNbArcs(graphe); ++i)
    {
        tArc arc = grapheRecupArcNumero(graphe, i);
        grapheRecupNomSommet(graphe, arc.orig, nomSommetOrig);
        grapheRecupNomSommet(graphe, arc.dest, nomSommetDest);
        fprintf(fic, "%s %s %s\n", nomSommetOrig, separator, nomSommetDest);
    }

    fprintf(fic, "}\n");

    fclose(fic);
    sprintf(commande, "dot -Tps %s -o %s.ps", dotfile, outfile);
    ret = system(commande);
    if (WEXITSTATUS(ret))
        halt("La commande suivante a echoue\n%s\n", commande);
}
void print_neighbourless_node(tGraphe graphe)
{
    tNomSommet nom;
    int i;

    for (i=0; i < grapheNbSommets(graphe);i++)
    {
        if (grapheNbVoisinsSommet(graphe, i) == 0)
        {
            grapheRecupNomSommet(graphe, i, nom);
            printf(" %s n'a pas de voisin \n", nom);
        }
    }
}

void print_node_with_max_neighbours(tGraphe graphe)
{
    tNomSommet nom;
    int nb_neighbours = 0;
    int max = 0;
    int i;

    for (i=0; i < grapheNbSommets(graphe);i++)
    {
        nb_neighbours = grapheNbVoisinsSommet(graphe, i);
        if (nb_neighbours > max)
        {
            max = nb_neighbours;
        }
    }

    for (i=0; i < grapheNbSommets(graphe);i++)
    {
        if (grapheNbVoisinsSommet(graphe, i) == max)
        {
            grapheRecupNomSommet(graphe, i, nom);
            printf("%s a le maximum de voisins avec %d voisins\n", nom, max);
        }
    }
}

long nbMicroSecondesDepuisDebutHeure() {
    struct timeval tv;
    long us;
    gettimeofday(&tv, NULL);
    // tv.tv_sec : nbre de secondes depuis Epoch
    // tv.tv_usec : complement en microsecondes
    tv.tv_sec = tv.tv_sec % 3600; // on fait un modulo une heure (=3600s)
    us = (tv.tv_sec*1000000)+tv.tv_usec;
    return us;
}

void buildGraphFromStack(tGraphe previous, tGraphe new, tPileSommets stack)
{
    grapheChangeType(new, 0);
    tNomSommet name;
    tNumeroSommet sommet;
    tArc arc;

    sommet = pileSommetsDepile(stack);
    grapheRecupNomSommet(previous, sommet, name); 
    grapheAjouteSommet(new, name);

    arc.orig = grapheChercheSommetParNom(new, name);

    do {
        sommet = pileSommetsDepile(stack);
        grapheRecupNomSommet(previous, sommet, name); 
        grapheAjouteSommet(new, name);
        arc.dest = grapheChercheSommetParNom(new, name);
        grapheAjouteArc(new, arc);
        arc.orig = arc.dest;
    } while (!pileSommetsEstVide(stack));

}

tPileSommets reverseStack(tPileSommets stack)
{
    tPileSommets new = pileSommetsAlloue();


    while(!pileSommetsEstVide(stack))
    {
        tNumeroSommet s = pileSommetsDepile(stack);
        pileSommetsEmpile(new, s);
    }
    return new;
}

int main(int argc, char *argv[]) {

    tGraphe graphe;
  //  tGraphe graphe2;
   // long time;
    //    tNomSommet nom;

    if (argc < 2)
    {
        halt("Usage : %s FichierGraphe\n", argv[0]);
    }

    graphe = grapheAlloue();
//    graphe2 = grapheAlloue();

    grapheChargeFichier(graphe,  argv[1]);
    // grapheAffiche(graphe);
    //print_neighbourless_node(graphe);
    //print_node_with_max_neighbours(graphe);

    //graphe_to_visual(graphe, argv[2]);

    //grapheRecupNomSommet(graphe, 0, nom);

    //breadth_first_search(graphe, nom);

    // shortestPath(graphe, 0);

    /*time = nbMicroSecondesDepuisDebutHeure();
      testShortestPath(graphe, atoi(argv[1]), 1);

      time = nbMicroSecondesDepuisDebutHeure() - time;

      printf("%ld\n", time);*/

    //    depthFirstSearch(graphe, 0);
/*
    tNumeroSommet entree = grapheChercheSommetParNom(graphe, "entree");
    tNumeroSommet sortie = grapheChercheSommetParNom(graphe, "sortie");
    tPileSommets stack = existsPathBetween(graphe, entree, sortie);
    if (stack != NULL)
    {
        stack = reverseStack(stack);
        buildGraphFromStack(graphe, graphe2, stack);
        graphe_to_visual(graphe2, argv[2]);
    }
    else
    {
        printf("No solution to this maze \n");
    }
*/
    dijkstra_algorithm(graphe, 0);

    grapheLibere(graphe);
   // grapheLibere(graphe2);
    return 0;
}
